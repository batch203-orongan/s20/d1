// console.log("Hello, World!");

// Repetition Control Structure (Loops)
	// Loops are one of the most important feature that programming must have.
	// It lets us execute code repeatedly in a pre-set number or maybe forever.

function greeting(){
	console.log("Hello My World!");
}

	// greetings();
	// greetings();
	// greetings();
	// greetings();
	// greetings();
	// greetings();
	// greetings();
	// greetings();
	// greetings();
	// greetings();
	// greetings();

	// let count = 10;

	// while(count !== 0){
	// 	console.log("This is printed inside the loop:" + count);
	// 	greeting();

	// 	// iteration
	// 	count --;
	// }

// [SECTION] While Loop
	// While loop allow us to repeat an action or an instruction as long as the condition is true.
	/*
		Syntax:
			while(expression/condition){
				// statement/code block

				finalExpression ++/-- (iteration)
			}

			- expressoin/condition => This are the unit of code that is being evaluated in our loop.Loop will run while the condition/expression is true.
			- statement/code block => code/instructions that will be executed several times.
			- finalExpression => indicates how to advance the loop.

	*/

	// let count = 5;

	// //  while the value of count is not equal to 0.
	// while(count !== 0){

	// 	console.log("Current value of count in the condition:" + count + " !== 0 is "+(count !==0));
		// console.log("While: " +count);

		// Decrease the value of count by 1 after every iterration of ur loop to stop it when it reaches to 0.
		// Forgetting to include this in loop will make our application run an infinite loop which will eventually crash our devices.
		// count--;
	// }

// [SECTION] Do while Loop
	// A do-while loop works a lot like the while loop. But unlike while loops do-while loops guarantee that the code will be executed atleast once.

	/*
	
		Syntax:
			do{
			// statement/code block

			finalExpression ++/--
			} while(expression/condition)

	*/
				// Number() can also convert a boolean or date.
	// let number = Number(prompt("Give me a number"));

	// do {
	// 	console.log("Do While: " +number);

	// 	// increase the value of number by 1 after every iteration to stop when reaches 10 or greater.
	// 	number += 1;
	// // Providing a number if 10 or greater will run the code block once and will stop the loop.
	// } while (number < 10);

	// while(number < 10){
	// 	console.log("While: "+ number);

	// 	number++
	// }


// [SECTION] for Loop
	// A for loop is more flexible than while and do-while loops.

	/*
		Syntax:
			for(initialization; expression/condition; finalExpression++/--){
				// statement/code block
			}

			Three parts of the for loop:
			1. Initialization => value that will track the progression of the loop.
			2. expression/condition => this will be evaluated to determin if the loop will run one more time.
			3. finalExpression => indicates how the loop advances.

	*/

	/*
		Mini Activity

			Refactor the code below that the loop will only print the even numbers.

	*/

	// for(let count = 0; count <= 20; count+=2){
	// 		console.log("For Loop: " +count);
	// }

	// for(let count = 0; count <= 20; count++){
	// 	if (count % 2 === 0) {
	// 		console.log("For Loop: " +count);
	// 	}
	// }

	/*
		let myString = "angelito"

		expected output:
		a
		n
		g
		e
		l
		i
		t
		o

	*/

	// Loop using String property
	// let myString = "emmanuel";

	// .length property is used to count the number of characters in a string.
	// console.log(myString.length);

	// Accessing element of a string
	// Individual characters of a string may be accessed using it's index number.
	// The first character in string corresponds to the number 0, to the nth number.
	// console.log(myString[0]); // to access the 1st element
	// console.log(myString[3]); // 4th character
	// console.log(myString[myString.length-1]); // to access the last character

	// Create a loop that will display the individual letters of my string in the console.

	// for(let x = 0; x < myString.length; x++){
	// 	console.log(myString[x]);
	// }

	/*
		Creates a loop that will pront out the  letters of the name individually and print out the number "3" instead when the letter to be printed out is a vowel.

	*/	

	// let myName = "TolIts"; // expected output: T 3 l 3 t s
	// let myNewName = ""

	// for (let i = 0; i < myName.length; i++){
	// 	// console.log(myName[i].toLowerCase());

	// 	// If the character of your name is a vaowel letter, instead of the character, it will display the nunber "3".

	// 	if(myName[i].toLowerCase() == 'a' || 
	// 		myName[i].toLowerCase() == 'e'|| 
	// 		myName[i].toLowerCase() == 'i'|| 
	// 		myName[i].toLowerCase() == 'o'|| 
	// 		myName[i].toLowerCase() == 'u'){
	// 		// vowels
	// 		console.log(3);

	// 		myNewName += 3;
	// 	}
	// 	else{
	// 		// Consonants
	// 		console.log(myName[i]);
	// 		myNewName += myName[i];
	// 	}
	// }
	// //concatenates the characters based on the given condition.
	// console.log(myNewName);

// [SECTION] Continue and Break Statements

/*
	- "continue" statements allows the code to go to the next iteration of the loop without finishing the execution of all the code block.
	- "break" statement is used to terminate the current loop once match has been found.
*/

/*
	Create a loop if the count values is divided by 2 and the remainder is 0, it will not print the number and continue to the next oteration of the loop. If the count value is greater than 10 it will stop the loop.

*/

	// for(let count = 0; count <= 20; count++){
	// 	// console.log(count);

	// 	if(count % 2 === 0){
	// 		continue;
	// 		// This ignores all statements located after the continue statement.
	// 		// console.log("Even "+count);
	// 	}
		
	// 		console.log("Continue and Break: "+count);

	// 		if(count > 10){
	// 			break;
	// 		}
	// 	}

/*

		Mini Activty:
		Creates a loop that will iterate based on the length of the string. If the current letter is equivalent to "a", print "Continue to the next iteration" and skip the current letter. If the current letter is equal to "d" stop the loop.

		let name = "Alexandro";

*/	
	
let name = "Alexandro"

for(let x = 0; x < name.length; x++){
		

		if(name[x].toLowerCase() == 'a'){
			console.log("Continue to the next iteration");
		}
		else if(name[x].toLowerCase() !== "a"){
			console.log(name[x]);
		}
		if(name[x].toLowerCase() == 'd'){
			break;
		}
	}
